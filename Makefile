#
#  Makefile for terraform commands
#

.PHONY: check init plan apply show graph ssh-keys

SUBDIR="nat-instance"
EC2_USERNAME=ec2-user

SSH_KEYFILE_NAT:=aws_ssh_nat
SSH_KEYFILE_CLIENT:=aws_ssh_client

all:
	@echo "Quick help screen:"
	@echo " make rules: make init plan apply show destroy graph"
	@echo " ssh test  : export EC2_HOSTNAME=blabla make do-ssh"


#
# Public rules
#
check:
	cd $(SUBDIR) && terraform fmt -check=true -diff=true

init: ssh-keys
	cd $(SUBDIR) && terraform init

plan:
	cd $(SUBDIR) && terraform plan

apply:
	cd $(SUBDIR) && terraform apply

show:
	cd $(SUBDIR) && terraform show

destroy:
	cd $(SUBDIR) && terraform destroy

graph:
	cd $(SUBDIR) && terraform graph | dot -Tsvg > ../graph.svg


#
# Internal rules
#
ssh-keys: $(SSH_KEYFILE_NAT) $(SSH_KEYFILE_CLIENT)

$(SSH_KEYFILE_NAT):
	ssh-keygen -t rsa -P "" -f $(SSH_KEYFILE_NAT)

$(SSH_KEYFILE_CLIENT):
	ssh-keygen -t rsa -P "" -f $(SSH_KEYFILE_CLIENT)

do-ssh:
	ssh $(EC2_USERNAME)@$(EC2_HOSTNAME) -i $(SSH_KEYFILE_NAT)

clean:
	find . -iname *~ -exec rm -rf {} \;
