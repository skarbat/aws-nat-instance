## AWS NAT Instance

This repository shows how to deploy a AWS NAT Instance on Amazon using Terraform.

### Running steps

The makefile provides all major Terraform steps for you, including the creation of the SSH key
for login into the AWS created instances.

You should export the Amazon key variables for Terraform to work correctly, an easy way is shown below:

```
AWS_ACCESS_KEY_ID=12345 AWS_SECRET_ACCESS_KEY=abcde make
```

#### References

 * Terraform tutorial: https://blog.gruntwork.io/an-introduction-to-terraform-f17df9c6d180#3fd2
 * Terraform and VPCs: https://medium.com/@hmalgewatta/setting-up-an-aws-ec2-instance-with-ssh-access-using-terraform-c336c812322f
 * Terraform testing: https://www.contino.io/insights/top-3-terraform-testing-strategies-for-ultra-reliable-infrastructure-as-code
 * Terraform gitlab pipeline: https://docs.gitlab.com/ee/user/infrastructure/
 * Similar project: https://github.com/int128/terraform-aws-nat-instance
 * Tutorial: https://www.kabisa.nl/tech/cost-saving-with-nat-instances/
