//servers.tf

resource "aws_key_pair" "nat_instance_key_pair" {
  key_name   = "ssh_key_1"
  public_key = file("../aws_ssh_nat.pub")
}

resource "aws_key_pair" "nat_client_key_pair" {
  key_name   = "ssh_key_2"
  public_key = file("../aws_ssh_client.pub")
}

resource "aws_instance" "test-nat-instance" {
  ami             = var.ami_id
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.nat_instance_key_pair.key_name
  security_groups = [aws_security_group.ingress-all-test.id]
  tags = {
    Name = var.ami_name_nat
  }
  subnet_id = aws_subnet.subnet-uno.id
}

resource "aws_instance" "test-nat-client" {
  ami                         = var.ami_id
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.nat_client_key_pair.key_name
  security_groups             = [aws_security_group.ingress-all-test.id]
  associate_public_ip_address = false
  tags = {
    Name = var.ami_name_client
  }
  subnet_id = aws_subnet.subnet-uno.id
}
