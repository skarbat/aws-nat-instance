//variables.tf

variable "ami_name_nat" {
  type    = string
  default = "nat-instance"
}

variable "ami_name_client" {
  type    = string
  default = "nat-client"
}

variable "ami_id" {
  type    = string
  default = "ami-01aab85a5e4a5a0fe"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}
